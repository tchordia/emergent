from emergent.core.base_model import BaseModel
import torch
import emergent.core.util as util
import emergent.core.vis as vis

class BinaryModel(BaseModel):

    def __init__(self, map_size):

        self.pixel_height = 5
        self.s_width = s_width = 3

        self.pixel = torch.rand(self.pixel_height, s_width, s_width, self.pixel_height) / 2

        for x in range(self.pixel_height):
            self.pixel[x, :, :, x] += torch.rand(s_width, s_width) / 2
            self.pixel[x, s_width//2, s_width//2, x] = .1

        map_size = map_size + (self.pixel_height,)
        self.next_pred = torch.zeros(*map_size)
        self.pred = self.next_pred.clone()
        self.state = self.next_pred.clone()
        self.last_state = self.state.clone()
        self.input = self.next_pred.clone()
        self.contrib = self.next_pred.clone()
        self.burst_state = self.next_pred.clone()
        self.weight_decay = .95


    def step(self, inp):

        in_map, loc = inp

        self.pred = self.next_pred.clone()
        self.last_state = self.state.clone()
        self.input = in_map

        def c(map):
            return util.grab_center(map, loc, in_map.size())

        bw_local = c(self.next_pred).clone()
        top_left = util.compute_top_left(loc, in_map.size())
        bw_local += in_map.unsqueeze(2)
        bw_local *= in_map.unsqueeze(2)

        all_active = bw_local * (bw_local == bw_local.max(dim=2, keepdim=True)[0]).float()
        burst_winners = torch.rand(all_active.size()) * all_active
        burst_winners = (burst_winners > 0).float() * (burst_winners == burst_winners.max(dim=2, keepdim=True)[0]).float()

        # copy the burst_winners as the new state
        map_center = c(self.state)
        map_center[:,:,:] = burst_winners

        # copy the full bursting state as the burst_state
        burst_all = c(self.burst_state)
        burst_all[:,:,:] = all_active

        # zero out the next prediction and the next contribution
        self.next_pred *= 0
        self.contrib = self.next_pred.clone()

        s = in_map.size()

        bw_local = burst_winners.clone()

        for x in range(s[0]):
            for y in range(s[1]):
                # p(local_map, 'lm')
                arr = bw_local[x, y, :]
                a = util.grab_center(self.next_pred, top_left + torch.tensor([x, y]), self.pixel.size()[1:3])
                a[:, :, :] = a + (arr * (self.pixel > .5).float()).sum(dim=0)


                contrib = util.grab_center(self.contrib, top_left + torch.tensor([x, y]), self.pixel.size()[1:3])
                contrib[:, :, :] = contrib + (arr * self.pixel).sum(dim=0)


        p(c(self.state), 'state')
        p(c(self.pred), 'last_pred')
        p(c(self.next_pred), 'map_pred')
        p(c(self.contrib), 'map_contrib')

        vis.plot(self.pred.sum(dim=2))

        return None

    def train(self, reward=None):

        update = .7
        bursting = self.burst_state.sum(dim=2, keepdim=True) > 1
        if (bursting.any()):
            idx = self.state.nonzero()[0]
            if (self.last_state > 0).any():
                idx_last = self.last_state.nonzero()[0]

                l = idx - idx_last
                if 1 >= l[0] >= -1 and 1 >= l[1] >= -1:
                    a = 1 - update * (1 - self.pixel[idx_last[2], 1 + l[0], 1 + l[1], idx[2]])
                    self.pixel[idx_last[2], 1 + l[0], 1 + l[1], idx[2]] = a

                self.pixel *= self.weight_decay
                self.weight_decay += .005
                self.weight_decay = min(.99, self.weight_decay)

                p(self.pixel[idx_last[2]])









        return

    def getLocal(self, inp):
        in_map, loc = inp

        return util.grab_center(self.next_pred, loc, in_map.size())

def p(map, label=''):
    for x in range(map.size()[2]):
        print(label, map[:,:,x])


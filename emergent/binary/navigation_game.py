from emergent.core.game import BaseGame
import torch
import emergent.core.vis as vis
from emergent.core.base_model import BaseModel
from emergent.binary.binaryseqmodel import BinaryModel
import emergent.core.util as util

class NavigationGame(BaseGame):

    game_size = 15, 15
    visible_size = 5, 5
    d_row = d_col = row = col = 5
    round = 0
    plot = True

    def default_state(self):
        a = torch.zeros(*self.game_size)
        a[self.row, self.col] = 1
        my_idx = torch.tensor(self.game_size) // 2
        return a, my_idx

    def act(self, action):
        map, loc = self.game_state
        map[self.row,self.col] = 0

        if (self.round // 3) % 2 == 0:
            self.row = self.d_row
            if self.col <= 10:
                self.col += 1
            else:
                self.col = 5
                self.round += 1
        else:
            self.col = self.d_col
            if self.row <= 10:
                self.row += 1
            else:
                self.row = 5
                self.round += 1



        map[self.row,self.col] = 1
        return (map, loc), 0, False

    def visible_state(self):

        map, loc = self.game_state

        vis_size = torch.tensor(self.visible_size)
        return util.grab_center(map, loc, vis_size), loc
        # return

    def draw(self, action, reward):
        a = self.visible_state()[0].clone().float()
        center = torch.tensor(self.visible_size) // 2
        # a[center[0], center[1]] = .25
        if self.plot:
            vis.plot(self.game_state[0])
        else:
            # print(a)
            pass

if __name__ == "__main__":
    player = BinaryModel((15, 15))
    n = NavigationGame(player)
    n.play(100)

    # local = player.getLocal(n.visible_state())
    # print(local[:,:,0])
    # player.step(n.visible_state())
    # local = player.getLocal(n.visible_state())
    # print(local[:,:,0])





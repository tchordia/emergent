from emergent.simple.seqmodel import BaseSeqModel


class BaseGame():

    def __init__(self, player :BaseSeqModel, warmup=0, num_steps_per_action=1, game_state=None, **kwargs):
        self.player = player
        self.init_sub(**kwargs)
        self.game_state = game_state if game_state else self.default_state()
        self.num_steps_per_action = num_steps_per_action
        self.num_steps_completed = 0
        self.warmup=warmup

    def step(self, draw=True, train=True):
        action = None
        for i in range(self.num_steps_per_action):
            action = self.player.step(self.visible_state())

        new_game_state, reward, game_over = self.act(action)

        self.game_state = new_game_state

        if train:
            self.player.train(reward=reward)

        if draw:
            self.draw(action, reward)
        self.num_steps_completed += 1
        return bool(game_over)

    def init_sub(self, **kwargs):
        pass

    def act(self, action) -> (object, float, bool):
        '''
        Define in subclass
        :return: new_game_state, and reward
        :rtype:
        '''
        return None, 0, False

    def action_space(self) -> (str, int):
        return None, None

    def draw(self, action, reward):
        '''
        Define in subclass
        :return:
        :rtype:
        '''
        pass

    def default_state(self):
        '''
        Define in subclass
        :return:
        :rtype:
        '''
        pass

    def visible_state(self, state=None):
        '''
        Override if necessary. by default tries to flatten state.
        :return:
        :rtype:
        '''
        return state.view(-1) if state is not None else self.game_state.view(-1)

    def play(self, maxiter=10):
        i = 0
        print("Starting game...")
        for i in range(self.warmup):
            self.step(train=False)
        while i < maxiter and not self.step():
            i+=1

        print("Game Finished after {} iterations".format(i))


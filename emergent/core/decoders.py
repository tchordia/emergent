import math
import torch

import emergent.core.util as util


class SimplePoolingDecoder(object):
    def __init__(self, input_length, output_length):
        self.group_length = math.ceil(input_length / output_length)

    def __call__(self, seqmodel, arr):
        tensors = torch.split(arr, self.group_length)

        means = torch.tensor([torch.mean(tensor) for tensor in tensors])
        out = torch.zeros(len(means))
        out[torch.argmax(means)] = 1
        return out

class ThresholdDecoder(object):
    def __init__(self, thresh=0):
        self.thresh = thresh

    def __call__(self, seqmodel, arr):
        out = (arr > self.thresh).float()
        return out

class NormalizationDecoder(object):
    def __init__(self):
        pass
    def __call__(self, seqmodel, arr):
        out = util.norm_array(arr)
        return out



import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import torch

fig = plt.figure()
def plot(arr):

    # i = plot.i
    # fig.add_subplot(i, 1, i)
    n = arr.numpy()
    plt.imshow(n)
    # plot.i += 1
    plt.colorbar()
    plt.show()


plot.i = 1


if __name__ == "__main__":
    plot(torch.randn(3, 3))
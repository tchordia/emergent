import torch as th
import torch.nn.functional as F
import numpy

class MatrixBuilder():


    def __init__(self, input_size, output_size):
        self.nn_init = 1 + input_size + output_size
        self.neurons = th.arange(self.nn_init)
        self.edge_mat = th.zeros(self.nn_init, self.nn_init)
        self.input_size = input_size
        self.output_size = output_size

        self.leak = 0

        self.layer_map = {
            'input': self.input_neurons(),
            'output': self.output_neurons()
        }

    def input_neurons(self):
        return self.neurons[1: 1 + self.input_size]

    def output_neurons(self):
        return self.neurons[1 + self.input_size : 1 + self.input_size + self.output_size]

    def addLayer(self, key, size):
        neurons = self.add_neurons(size)
        self.layer_map[key] = neurons
        return neurons

    def random_connect(self, p, n_in, n_out):

        x, y = cartesian_product(n_in, n_out)
        self.edge_mat[y, x] = (th.rand(len(n_in) * len(n_out)) < p).float()

    def randomize_weights(self):
        self.edge_mat = self.edge_mat * th.rand(self.edge_mat.size())
        self.set_leak(self.leak)


    def add_neurons(self, n):
        self.edge_mat = F.pad(self.edge_mat, (0, n, 0, n), 'constant', 0)
        self.neurons = th.arange(len(self.neurons) + n)
        return self.neurons[-n:]

    def set_leak(self, leak, neurons=None):
        self.leak = leak
        neurons = neurons if neurons is not None else self.neurons
        self.edge_mat[neurons, 0] = leak

    def trainabilityMask(self):
        temp = (self.edge_mat != 0).float()
        # bias is not trainable
        temp[:, 0] = 0
        return temp


def cartesian_product(x, y):
    l_x = len(x)
    l_y = len(y)
    q = x.view([-1, 1]).repeat(1, l_y).view(1, -1)
    w = y.view([1, -1]).repeat(l_x, 1).view(1, -1)

    return q, w

if __name__ == "__main__":
    m = MatrixBuilder(3, 3)

    # l1 = m.addLayer('l1', 2)

    # m.random_connect(1, m.input_neurons(), l1)
    # m.random_connect(1, l1, m.output_neurons())
    m.random_connect(1, m.input_neurons(), m.output_neurons())

    m.set_leak(-.25)
    print(m.edge_mat)
    print(m.trainabilityMask())



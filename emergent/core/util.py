import torch

def row_mat(arr):
    return arr.view(1, -1)

def col_mat(arr):
    return arr.view(-1, 1)

def z_mat(arr):
    return arr.view(1, 1, -1)

def norm_mat(mat):
    return mat / col_mat(mat.sum(1))

def norm_array(arr):
    s = arr.sum()

    return arr if s == 0 else arr / s

def grab_center(map, location, vis_size):

    vis_size = torch.tensor(vis_size)
    corner = location - vis_size // 2
    other_corner = vis_size + corner
    return map[corner[0]: other_corner[0], corner[1]: other_corner[1]]

def compute_top_left(location, square_size):
    square_size = torch.tensor(square_size)

    return location - square_size // 2


class BaseModel(object):
    """
    The base format for models
    """

    def __init__(self):
        pass

    def step(self, input):
        """

        :return: Return the output of this model on this input after stepping
        :rtype:
        """

        return input

    def train(self, reward=None):
        """
        Train with this reward
        :param reward:
        :type reward:
        :return:
        :rtype:
        """


        return True

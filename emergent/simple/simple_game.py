import torch as th

import emergent.core.decoders as dec
from emergent.core.game import BaseGame
from emergent.core.matrix_builder import MatrixBuilder
from emergent.simple.seqmodel import BaseSeqModel


class SimpleGame(BaseGame):

    def init_sub(self, size=3):
        self.size = size

    def default_state(self):
        a = (th.rand(self.size) < .5).int()
        print('initial_state', a)
        return a

    def act(self, action):

        new_state = th.clamp(self.game_state ^ action.int(), min=0, max=1)
        reward = int(th.sum(self.game_state.int() * action.int()) - .5 * th.sum((action.int() > self.game_state.int()).int()) )

        if self.num_steps_completed % 10 == 0:
            new_state += th.clamp((th.rand(new_state.size()) < .1).int(), min=0, max=1)


        return new_state, reward, th.sum(new_state) == 0

    def draw(self, action, reward):
        print('action', action.view(3, -1))
        print('state', self.game_state.view(3, -1))
        print('reward', reward)

    def action_space(self):
        return 'tensor', self.size ** 2

    def visible_state_space(self):
        return 'tensor', self.size ** 2

class SimpleGameContinuous(BaseGame):

    def init_sub(self, size=3):
        self.size = size

    def default_state(self):
        a = th.rand(self.size)
        print('initial_state', a)
        return a

    def act(self, action):

        new_state = self.game_state - action
        reward = th.sum((self.game_state - new_state) ** 2)

        return new_state, reward, (new_state < 0.01).all()

    def draw(self, action, reward):
        print('action', action.view(3, -1))
        print('state', self.game_state.view(3, -1))
        print('reward', reward)

if __name__ == "__main__":
    seq_in = 9
    seq_out = 9
    game_size = 9
    leak = -.1

    th.manual_seed(5)
    m = MatrixBuilder(seq_in, seq_out)
    m.random_connect(1, m.input_neurons(), m.output_neurons())
    m.randomize_weights()
    m.set_leak(leak)

    print(m.edge_mat)


    b = BaseSeqModel(m.neurons, m.edge_mat, m.input_size, m.output_size, m.trainabilityMask(),
                     decode = dec.NormalizationDecoder()
        )
    s = SimpleGameContinuous(b, warmup=10, num_steps_per_action=1, size=game_size)

    s.play(100)
    print(th.trunc(s.player.edge_mat[10:, 1:10] * 100))
    print(s.player.edge_mat.sum(1))
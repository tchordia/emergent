import torch
import torch.distributions.bernoulli as bern
from types import MethodType



class BaseSeqModel():
    '''
    Expected neuron format: 0th element is fixed to 1. Next n elements are the input. Last m elements are output.
    '''

    leak = .1               # how fast a neuron loses it's state
    update_alpha = 1        # how fast the edge matrix is trained
    history_length = 10      # how much history we store
    fire_thresh = 1         # threshold after which the default non_linearity fires

    contribution_decay = 2  # the contribution history is multiplied by this value ** (- how_long_ago)
                            # eg: .5, .25, ... multiplier for contributions 1, 2,... time steps ago

    reward_gen = bern.Bernoulli(torch.tensor([0.3]))

    def __init__(self, neurons, edge_mat, input_size, output_size, trainability_mask=None, encode=None, decode=None, non_lin=None):
        self.neurons = neurons
        self.edge_mat = edge_mat
        self.base_trainability_mask = trainability_mask if trainability_mask is not None else (edge_mat != 0).float()

        self.neuron_state_history = torch.randn(len(neurons), self.history_length)
        self.neuron_firing_history = torch.zeros(len(neurons), self.history_length)
        self.neuron_contribution_history = torch.zeros(edge_mat.size() + (self.history_length,))

        self.neuron_firing_history[:, 0], self.neuron_state_history[:, 0] = self.non_lin(self.n_state())

        self.input_size = input_size
        self.output_size = output_size

        self.num_neurons = len(self.neurons)

        self.encode = MethodType(encode, self) if encode else self.encode
        self.decode = MethodType(decode, self) if decode else self.decode
        self.non_lin = MethodType(non_lin, self) if non_lin else self.non_lin

    def step(self, input):

        print("")
        print("Step.... \n", input.view(3, 3))
        # Set the input.
        fired = self.n_fired().clone()

        # encode input and bias
        fired[1:1 + len(input)] = self.encode(input)
        fired[0] = 1

        # update the previous history to show the new input
        self.n_fired(update=fired)


        print("Fired\n", fired)

        # calculate contribution matrix
        n_contribution = self.edge_mat * row_mat(fired)

        # feed forward
        new_state = torch.sum(n_contribution, dim = 1) + self.n_state()

        print("New state\n", new_state)

        # Feed forward alternate
        # new_state = self.edge_mat @ self.n_fired() # + self.self_decay * state if you want self_decay in the future

        n_fired, new_state = self.non_lin(new_state)

        # encode bias
        new_state[0] = n_fired[0] = 1

        print("Post nonlin\n", n_fired, new_state)

        self.update_history(n_fired, new_state, n_contribution)

        print(self.output_neurons(n_fired))
        return self.decode(self.output_neurons(n_fired))

    def update_history(self, n_fire, n_state, n_contribution):
        self.neuron_firing_history = torch.cat((col_mat(n_fire), self.neuron_firing_history[:, :-1]), dim=1)
        self.neuron_state_history = torch.cat((col_mat(n_state), self.neuron_state_history[:, :-1]), dim=1)
        self.neuron_contribution_history = torch.cat((n_contribution.unsqueeze(2), self.neuron_contribution_history[:, :, :-1]), dim=2)

    def input_neurons(self, neurons):
        return neurons[1: 1 + self.input_size]

    def output_neurons(self, neurons):
        return neurons[1 + self.input_size : 1 + self.input_size + self.output_size]

    def n_state(self, index=0):
        return self.neuron_state_history[:, index]

    def n_fired(self, index=0, update=None):

        if update is not None:
            self.neuron_firing_history[:, index] = update
        return self.neuron_firing_history[:, index]


    def train(self, reward=None):
        '''
        Grab the list

        stuff to implement:
        random firing of some neurons

        :param reward:
        :type reward:
        :return:
        :rtype:
        '''

        # multiplier with exponential decay
        update_multiplier = self.contribution_decay ** -torch.arange(0., self.history_length)
        update_multiplier[0] = 0

        # change contribution sum to incorporate firing history
        # in order to allow negative contributions to train correctly.
        # ex: a negative contribution when you don't fire is good
        # still only train on fires
        # contribution history format: [receiver, sender, time point]

        # 1 if fired, -1 if didn't fire
        # shape [num_neurons, 1, history_length]
        bool_fire_history = (2 * (self.neuron_firing_history > 0).float() - 1).unsqueeze(1)

        contribution_sum = (update_multiplier * bool_fire_history * self.neuron_contribution_history).sum(dim=2)
        print(contribution_sum[10:, 1:10])

        reward_multiplier = reward if reward != 0 else self.reward_gen.sample()

        trainability_mask = self.base_trainability_mask * col_mat((self.n_fired() > 0).float())
        self.edge_mat = norm_mat((1 + (reward_multiplier * self.update_alpha * trainability_mask * contribution_sum)) * self.edge_mat)

        return self.edge_mat

    def non_lin(self, states):

        n_fired = torch.clamp(states - self.fire_thresh, min=0, max=1)
        new_state = torch.clamp(states * (n_fired == 0).float(), min=0, max=self.fire_thresh)

        return n_fired, new_state

    def encode(self, input_vector):
        return input_vector

    def decode(self, output_vector):
        return (output_vector > 0).float()


def row_mat(arr):
    return arr.view(1, -1)

def col_mat(arr):
    return arr.view(-1, 1)

def z_mat(arr):
    return arr.view(1, 1, -1)

def norm_mat(mat):
    return mat / col_mat(mat.sum(1))

if __name__ == "__main__":
    # b = BaseSeqModel(list(range(10)), torch.randn(10, 10), 2, 2)
    b = BaseSeqModel(list(range(10)), torch.randn(10, 10), 2, 2)

    print(b.edge_mat)
    print(b.train(5))

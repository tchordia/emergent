import numpy as np
import emergent.const as const

class Neuron:

    global_id = 1
    global_map = {}


    def __init__(self, in_weight, out_weight, in_thresh = .5, out_thresh = .5):

        # Register the neuron globally by its id
        self.id = self.global_id
        Neuron.global_id += 1
        Neuron.global_map[self.id] = self

        self.in_thresh = in_thresh
        self.out_thresh = out_thresh

        self.out_weight = out_weight.copy()
        self.out_active = out_weight > self.out_thresh
        self.out_avail = out_weight > 0

        self.in_weight = in_weight.copy()
        self.in_active = in_weight > self.in_thresh
        self.in_avail = in_weight > 0

        self.in_field_size = len(in_weight)
        self.out_field_size = len(out_weight)
        self.update_rate = const.update_rate



    @staticmethod
    def generate_uniform_weights(size, percent_avail):
        idx = np.random.permutation(size)[:round(size*(1 - percent_avail))]
        b = np.random.rand(size)
        b[idx] = 0
        return b

    @staticmethod
    def generate_random_neuron(input_field_size, output_field_size, input_avail, output_avail):
        return Neuron(
                    Neuron.generate_uniform_weights(input_field_size, input_avail),
                    Neuron.generate_uniform_weights(output_field_size, output_avail)
        )

    def phase_1(self, raw_input):
        return raw_input.dot(self.in_weight * self.in_active)

    def learn_input(self, raw_input):
        weight = self.in_weight.copy()
        idx = np.where(raw_input * self.in_avail)
        nidx = np.where((1 - raw_input) * self.in_avail)
        self.in_weight[idx] = self.update_rate * self.in_weight[idx] + (1 - self.update_rate)
        self.in_weight[nidx] = self.update_rate * self.in_weight[nidx]

        self.update_in(self.in_weight)


    def phase_2(self):
        return self.out_active.astype(int)

    def learn_output(self, correct_output):
        idx = np.where(correct_output * self.out_avail)
        nidx = np.where((1 - correct_output) * self.out_avail)
        self.out_weight[idx] = self.update_rate * self.out_weight[idx] + (1 - self.update_rate)
        self.out_weight[nidx] = self.update_rate * self.out_weight[nidx]

        self.update_out(self.out_weight)

    def update_out(self, out_weight):
        self.out_active = out_weight > self.out_thresh
        self.out_avail = out_weight > 0

    def update_in(self, in_weight):
        self.in_active = in_weight > self.in_thresh
        self.in_avail = in_weight > 0



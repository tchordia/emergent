from emergent import params
input_avail = .85
output_avail = 1
update_rate = .8

default_region_params_dict = {
    'input_avail' : .85,
    'output_avail' : 1,
    'p_to_act' : .2,
    'output_thresh': .5
}

default_region_params = params.getParams('Region', default_region_params_dict)


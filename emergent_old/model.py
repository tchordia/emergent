import numpy as np
import sklearn.preprocessing as pre

class Model():

    """

    The fields that are "_h" are hyperparams. The "coeffs" are params set by the program.

    """
    # Hyper params
    update_h = .5
    distal_reduction_h = .1
    firing_mult_h = 2
    matrix_norm_h = 'l1'

    def __init__(self, state, feed_forward_matrix, distal_matrix, block_size, leak):
        self.state = state
        self.block_size = block_size
        self.leak = leak

        self.feed_forward_matrix = self.norm(feed_forward_matrix)
        self.distal_matrix = self.norm(distal_matrix)

        # find the visibility fields fields
        self.ff_field = self.feed_forward_matrix != 0
        self.distal_field = self.distal_matrix != 0

        # Nothing starts out active.
        self.active = np.zeros(state.shape)

        # Initialize the parameters
        self.ff_mean_field = ff_mean_field = self.ff_field.sum(axis=1).mean()
        self.distal_mean_field = distal_mean_field = self.distal_field.sum(axis=1).mean()

        self.update_coeff = self.update_h / ff_mean_field
        self.distal_coeff = self.distal_reduction_h * self.update_h / distal_mean_field
        self.firing_thresh_coeff = self.firing_mult_h * ff_mean_field

    def step_forward_integrate(self):
        new_state, new_active = self.feed_forward_integrate()
        new_ff, new_distal = self.train_integrate(new_active)
        self.commit_integrate(new_state, new_ff, new_distal, new_active)

    def feed_forward_integrate(self):
        new_state = self.state + self.feed_forward_matrix.dot(self.active) - self.leak

        blocks = new_state.reshape([-1, self.block_size])
        block_thresh = np.maximum(np.max(blocks, axis = 1), self.firing_thresh_coeff)

        new_active = (blocks >= self.col_mat(block_thresh)).flatten()

        # Clear the state for active neurons
        new_state[new_active] = 0

        # Preactivate with distal input
        new_state += self.distal_coeff * self.distal_matrix.dot(new_active)

        return new_state, new_active

    def train_integrate(self, new_active):

        update_arr = 2 * self.update_coeff * self.active - self.update_coeff
        update_arr, new_active = self.row_mat(update_arr), self.col_mat(new_active)

        # update_arr is a row vector, new_state is a column vector
        # If the new_state is 1, then update that row with update_arr.
        # The below code will broadcast them correctly

        new_ff = self.norm((self.feed_forward_matrix * new_active + update_arr) * self.ff_field)
        new_distal = self.norm((self.distal_matrix * new_active + update_arr) * self.distal_field)
        return new_ff, new_distal

    def commit_integrate(self, ns, mat_ff, mat_dist, active):
        self.state = ns
        self.feed_forward_matrix = mat_ff
        self.distal_matrix = mat_dist
        self.active = active

    def norm(self, mat):
        return pre.normalize(mat, norm = self.matrix_norm_h)

    def row_mat(self, arr):
        return arr.reshape([1, -1])

    def col_mat(self, arr):
        return arr.reshape([-1, 1])

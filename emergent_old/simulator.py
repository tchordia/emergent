import emergent as em
import emergent.const as const

import numpy as np
import itertools

import random

import logging as lg
import pprint

class Simulator():

    def __init__(self, num_neurons, tr_input, tr_output, te_input, te_output, loop = False):

        self.step = 0
        self.input_sequence = iter(tr_input)
        self.output_sequence = iter(tr_output)
        self.loop = loop

        self.input_size = len(tr_input[0])
        self.output_size = len(tr_output[0])
        self.num_neurons = num_neurons

        self.te_in = iter(te_input)
        self.te_out = iter(te_output)

    def run(self):

        region = em.Region(self.num_neurons, self.input_size, self.output_size, const.default_region_params)

        count = 0

        final_train = []
        final_test = []
        while self.loop or count == 0:
            count += 1

            for input in self.input_sequence:
                self.step += 1
                out = next(self.output_sequence)
                pred, idx = region.step(input, out)
                lg.info("Step %s, out %s", self.step, pred)

                final_train.append((input, pred, out))

        for input in self.te_in:
            out = next(self.te_out)

            pred, idx = region.step(input, out, train=False)
            overlap = sum(pred * out)
            missed = sum(pred | out) - overlap
            final_test.append((input, out, pred, overlap, missed, idx))


        return final_train, final_test



def setup():

    lg.basicConfig(level=lg.DEBUG)
    random.seed(1)
    np.random.seed(1)

def main():

    setup()

    pp = pprint.PrettyPrinter(indent=4)

    start = np.asarray([1,1,1,1,0,0,0,0,0,0,0])
    tr_input = [np.roll(start, 3 * (x % 10)) for x in range(1000)]
    tr_output = list(reversed(tr_input))

    te_input = tr_input[:10]
    te_output = tr_output[:10]

    simulator = Simulator(20, tr_input, tr_output, te_input, te_output)
    finaltr, finalte = simulator.run()

    pp.pprint(finalte)

if __name__ == "__main__":
    main()






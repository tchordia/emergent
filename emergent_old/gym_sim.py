import gym

env_name = 'CartPole-v0'

env = gym.make(env_name)
env.reset()

print(env.action_space)
print(env.observation_space)



for _ in range(1000):
    env.render()
    obs, reward, done, info = env.step(env.action_space.sample()) # take a random action

    if done:
        env.reset()


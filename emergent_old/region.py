from emergent import Neuron

import logging as lg
import numpy as np

class Region():

    def __init__(self, num_neurons, input_field_size, output_field_size, params):

        self.neurons = [Neuron.generate_random_neuron(input_field_size, output_field_size, params.input_avail, params.output_avail) for x in range(num_neurons)]
        self.num_to_activate = round(params.p_to_act * num_neurons)
        self.params = params


    def step(self, input_field, output_field, train = True):
        scores = np.asarray([neuron.phase_1(input_field) for neuron in self.neurons])

        lg.info("First input scores, %s", scores)

        idx = np.argsort(scores)[-self.num_to_activate:]
        lg.info("Top performing neurons, %s", idx)

        active = [self.neurons[i] for i in idx]

        lg.info(active)

        output = (sum([n.phase_2() for n in active]) / len(active) > self.params.output_thresh).astype(int)

        lg.info("Output: %s", output)

        if train:
            for a_n in active:
                a_n.learn_input(input_field)

            for a_n in active:
                a_n.learn_output(output_field)

        return output, idx












from collections import namedtuple

def getParams(tag, default_params, **override):

    params = dict(default_params)

    for key in override:
        params[key] = override[key]

    return namedtuple(tag, params.keys())(*params.values())







